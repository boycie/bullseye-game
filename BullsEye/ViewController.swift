//
//  ViewController.swift
//  BullsEye
//
//  Created by Bojan Petkovic on 10/09/2018.
//  Copyright © 2018 Bojan Petkovic. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var currentValue: Int = 0
    var targetValue: Int = 0
    var points: Int = 0
    var gameRound: Int = 0
    
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var targetValueLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var roundLabel: UILabel!
    @IBOutlet weak var startOverButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        prepareGame()
        startNewRound()
        print("Slider value on didLoad is: \(slider.value)")
        
        let thumbImageNormal = UIImage(named: "SliderThumb-Normal")
        slider.setThumbImage(thumbImageNormal, for: .normal)
        
        let thumbImageHighlighted = UIImage(named: "SliderThumb-Highlighted")
        slider.setThumbImage(thumbImageNormal, for: .highlighted)
        
        let insets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
        
        let trackLeftImage = UIImage(named: "SliderTrackLeft")
        let trackLeftResizable = trackLeftImage?.resizableImage(withCapInsets: insets)
        slider.setMinimumTrackImage(trackLeftImage, for: .normal)
        
        let trackRightImage = UIImage(named: "SliderTrackRight")
        let trackrightResizable = trackRightImage?.resizableImage(withCapInsets: insets)
        slider.setMaximumTrackImage(trackRightImage, for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func showAlert() {
        let difference: Int = abs(currentValue - targetValue)
        let score = 100 - difference
        points = points + score
        
        let title: String
        if difference == 0 {
            title = "Perfect"
            points += 100
        } else if difference == 1 {
            title = "Perfect"
            points += 50
        } else if difference < 5 {
            title = "Almost"
        } else {
            title = "Far far away"
        }
        
        let message = "You scored \(score)"
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Awesome", style: .default) { (action) in
            self.startNewRound()
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func sliderMoved(_ slider: UISlider) {
        currentValue = Int(round(slider.value * 100))
        print("Slider value is \(currentValue)")
    }
    
    @IBAction func restartGame(_ sender: UIButton) {
        points = 0
        gameRound = 1
        prepareGame()
    }
    
    func prepareGame() {
        setScore()
        setRound()
    }
    
    func updateLabels() {
        targetValueLabel.text = String(targetValue)
        prepareGame()
    }
    
    func setScore() {
        pointsLabel.text = String(points)
    }
    
    func setRound() {
        roundLabel.text = String(gameRound)
    }
    
    func startNewRound() {
        targetValue = 1 + Int(arc4random_uniform(100))
        currentValue = 50
        let sliderValue: Float = Float(currentValue) / 100
        slider.value = sliderValue
        print("Current value in newRound is \(Float(currentValue))")
        gameRound += 1
        updateLabels()
    }
}

